package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sort"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type intSet map[int]struct{}

func (l intSet) String() string {
	var tmp []string
	for i := range l {
		tmp = append(tmp, strconv.Itoa(i))
	}
	return strings.Join(tmp, ",")
}

func (l intSet) Set(value string) error {
	n, err := strconv.Atoi(value)
	if err != nil {
		return err
	}
	l[n] = struct{}{}
	return nil
}

func (l intSet) Values() []int {
	var out []int
	for i := range l {
		out = append(out, i)
	}
	return out
}

var (
	portSet  = make(intSet)
	httpPort = flag.Int("http-port", 9419, "port for the HTTP server")
)

func init() {
	flag.Var(portSet, "port", "port to listen on (can be specified multiple times)")
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	ports := portSet.Values()
	if len(ports) == 0 {
		log.Fatal("no ports specified!")
	}
	sort.Ints(ports)

	http.Handle("/metrics", promhttp.Handler())
	srv := &http.Server{
		Addr:         fmt.Sprintf(":%d", *httpPort),
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		IdleTimeout:  300 * time.Second,
	}
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
		}
	}()

	stopCh := make(chan struct{})
	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("terminating")
		close(stopCh)
		srv.Shutdown(context.Background())
	}()
	signal.Notify(sigCh, syscall.SIGTERM, syscall.SIGINT)

	if err := hark(ports, stopCh); err != nil {
		log.Fatal("error: %v", err)
	}
}
