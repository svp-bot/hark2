package main

import (
	"fmt"
	"log"
	"net"
	"strconv"
	"strings"
	"sync"

	"github.com/prometheus/client_golang/prometheus"
)

var connections = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "unexpected_connections",
		Help: "Unexpected connection counter.",
	},
	[]string{"port"},
)

func init() {
	prometheus.MustRegister(connections)
}

func listener(l net.Listener, port int) {
	for {
		conn, err := l.Accept()
		if err != nil {
			return
		}

		log.Printf("connection on port %d from %s", port, conn.RemoteAddr().String())

		connections.WithLabelValues(strconv.Itoa(port)).Inc()

		conn.Close()
	}
}

func hark(ports []int, stopCh chan struct{}) error {
	var wg sync.WaitGroup
	var listeners []net.Listener
	for _, port := range ports {
		l, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
		if err != nil {
			return fmt.Errorf("can't listen on port %d: %v", port, err)
		}
		listeners = append(listeners, l)

		// Reset the counter explicitly so the metrics appear even
		// before the first connection.
		connections.WithLabelValues(strconv.Itoa(port)).Add(0)

		wg.Add(1)
		go func(port int) {
			listener(l, port)
			wg.Done()
		}(port)
	}

	log.Printf("listening on ports %s", intListToString(ports))

	<-stopCh

	for _, l := range listeners {
		l.Close()
	}

	wg.Wait()

	return nil
}

func intListToString(l []int) string {
	var tmp []string
	for _, i := range l {
		tmp = append(tmp, strconv.Itoa(i))
	}
	return strings.Join(tmp, ", ")
}
